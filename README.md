# OLED_SSD1306_STM8L

## Library Info

This is modified version of the [Zbigniew Gibek SSD1306 OLED library](https://github.com/microgeek-eu/oled_ssd1306_stm8) for use with STM8L052C6T6 MCU using STVD IDE with COSMIC compiler in Windows 10 x64.
More information about the library can be found in the [original repo readme](https://github.com/microgeek-eu/oled_ssd1306_stm8/blob/master/README.md).

This library is compatible with ST Visual Develop IDE with COSMIC compiler.
The library was used for (Uni4Kids HandWatch project)[https://gitlab.com/uni4kids/hand-watch] and only the functions used in that project are tested, other functions still need to be tested and fixed(mostly variable declarations as described in [CHANGELOG.md](/CHANGELOG.md)) to be fully compatible.

#### Contributors

[CONTRIBUTING](/CONTRIBUTING.md)

#### Changelog

[CHANGELOG](/CHANGELOG.md)

#### Library status
Mostly finished modifications.

#### License
The modified library is licensed under the [MIT license](/LICENSE) and everyone can use it for free.

