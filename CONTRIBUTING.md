# Library contributors

[Ivan Stefanov](https://gitlab.com/ivan.stefanov.513) ([Email](mailto:ivan.stefanov.513@gmail.com?subject=[Gitlab]OLED%20Library)) Uploaded version 1.0.0 on 31.10.2021

[Mateusz Salamon](mailto:mateusz@msalamon.pl) Original STM32 OLED library creator 2018

[Zbigniew Gibek](mailto:zbeegin@op.pl) Modified the library for use with STM8 2019

