# Library Changelog

## Generic information

In all files `#include <stdint.h>` is commented out because when using COSMIC compiler int type definitions are taken from the MCU header file(for exampe `stm8l15x.h`). If using SDCC compiler you need to patch the SPL(ST Standard Peripherial Library) for use with it as is described in the [oled_ssd1306_stm8 repo](https://github.com/microgeek-eu/oled_ssd1306_stm8) from where this library was taken. In this library the SPL is the non-patched official version from the ST Microelectronics website.

This library is compatible with ST Visual Develop IDE with COSMIC compiler.
The library was used for (Uni4Kids HandWatch project)[https://gitlab.com/uni4kids/hand-watch] and only the functions used in that project are tested, other functions still need to be tested and fixed(mostly variable declarations as described below) to be fully compatible.

## 1.0.0 (31.10.2021)

### Display resolution change

Original library was setup for use with display SSD1306 OLED 128x64 some parameters was changed for SSD1306 OLED 128x32 version:


In `oled_ssd1306.h` changed:
```C++
	#define SSD1306_LCDHEIGHT  32		// Changed display height definition
```

In `oled_ssd1306.c` added:
```C++
/**
  * @brief  SSD1306 OLED Controller page addresses
  */
#define SSD1306_PAGE_START           0x00
#define SSD1306_PAGE_END             0x03	// For 128x64 use 0x07 | For 128x32 use 0x03
```

In `oled_ssd1306.c` changed:
```C++
void SSD1306_Init(void)
{
	...

	SSD1306_SetContrast(0x7F); 		// Changed default contrast from 0xFF to 0x7F
	
	...

	SSD1306_Command(SSD1306_SETMULTIPLEX);
	/* Default => 0x3F (1/64 Duty)	0x1F(1/32 Duty) */
	SSD1306_Command(0x1F); 			// For 128x64 use 0x3F | For 128x32 use 0x1F

	...

	/* Set COM Hardware Configuration */
	SSD1306_Command(SSD1306_SETCOMPINS);
	SSD1306_Command(0x02); 			// For 128x64 use 0x12 | For 128x32 use 0x02

	...
}

void SSD1306_Display(void)
{
	SSD1306_Command(SSD1306_PAGEADDR);
	SSD1306_Command(SSD1306_PAGE_START);	// Was SSD1306_Command(0x00);
	SSD1306_Command(SSD1306_PAGE_END);		// Was SSD1306_Command(0x07);

	...
}

void SSD1306_Bitmap(uint8_t *bitmap)
{
	SSD1306_Command(SSD1306_PAGEADDR);
	SSD1306_Command(SSD1306_PAGE_START);	// Was SSD1306_Command(0x00);
	SSD1306_Command(SSD1306_PAGE_END);		// Was SSD1306_Command(0x07);

	...
}


```

### C89 compatibility fix

Original code cannot be compiled with COSMIC compiler because it supports only C89 and two changes are needed for the code to compile properly:
- There cannot be variable declarations in the loop statements
	* `for (int i = 0; ...)` is a syntax introduced in C99
- All of the variable declarations must be at the beginning of the block
	* C99 and later allow variable declarations to be placed anywhere in the block

[Stackoverflow reference for the above information](https://stackoverflow.com/questions/1287863/c-for-loop-int-initial-declaration)

The functions which need to be changed are in the `gfx_bw.c` file.
- Functions with moved variable declarations at the beginning outside of for() statement:
	* `GFX_WriteLine`,`GFX_DrawFillRectangle`,`GFX_DrawChar` are fixed
	* the other functions need to be fixed

### Functional changes

In file `gfx_bw.c`:
- Function `GFX_DrawChar`:
	* Removed `USE_STRETCH_FONTS` option because in COSMIC there was some problem with it and the preprocessor statements `#if #else #endif` weren't processed it properly and font size larger than 1 only increased the spacing between the characters. The option was useless anyway as with `size=1` there still isn't any difference in code behaviour. With larger size filled square with x=y=size is drawn instead of a single pixel. The library only works with integer font sizes.
- Function `GFX_DrawString`:
	* Increased the empty space between characters as it was fixed 1 vertical line and when using larger size fonts the characters were too close. Now there are N empty vertical lines between characters and `N = size + 1`(for example size 4 with have 5 empty vertical lines): 
	```C++
		x_tmp += ((uint8_t)font[1] + 1) * size + 1;
		GFX_DrawFillRectangle(x_tmp - (size + 1), y + i, size, size, PIXEL_BLACK);
	```