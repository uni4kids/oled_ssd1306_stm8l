/**
  ****************************************************************************
  * @file           : oled_hw_setup.c
  * @brief          : This file contains the hardware setup routines.
  **************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
//#include <stdint.h>
#include <stdlib.h>
#include "stm8l15x.h"
#include "oled_hw_setup.h"

/**
  * @brief Used GPIO Configuration
  * @retval None
  */
void OLED_GPIO_Config(void)
{
  /* Configure GPIO pins */
#ifdef SSD1306_RESET_USE
  GPIO_Init(SSD1306_RESET_Port, SSD1306_RESET_Pin, GPIO_Mode_Out_PP_High_Slow);
#endif

#ifdef SSD1306_SPI_CONTROL
  /* Configure the SCK, MOSI, MISO */
  GPIO_Init(SSD1306_SCK_Port,   SSD1306_SCK_Pin,   GPIO_Mode_Out_PP_Low_Fast);
  GPIO_Init(SSD1306_MOSI_Port,  SSD1306_MOSI_Pin,  GPIO_Mode_Out_PP_Low_Fast);
  GPIO_Init(SSD1306_MISO_Port,  SSD1306_MISO_Pin,  GPIO_MODE_In_PU_No_IT);
  /* Configure the SS pin */
  GPIO_Init(SSD1306_CS_Port,    SSD1306_CS_Pin,    GPIO_Mode_Out_PP_High_Fast);
  /* Configure the D/C pin */
  GPIO_Init(SSD1306_DC_Port,    SSD1306_DC_Pin,    GPIO_Mode_Out_PP_Low_Slow);
#endif

#ifdef SSD1306_I2C_CONTROL
  /* Configure the SCL and SDA pins */
  GPIO_Init(SSD1306_SCL_Port,   SSD1306_SCL_Pin,   GPIO_Mode_Out_OD_HiZ_Fast);
  GPIO_Init(SSD1306_SDA_Port,   SSD1306_SDA_Pin,   GPIO_Mode_Out_OD_HiZ_Fast);
#endif
}

#ifdef SSD1306_I2C_CONTROL
/**
  * @brief I2C Peripherial Configuration
  * @retval None
  */
void I2C_Config(void)
{
  I2C_DeInit(I2C1);

  /* Set the I2C transmit parameters */
  I2C_Init(I2C1,
           SSD1306_I2C_SPEED,
           0x01,
           I2C_Mode_I2C,
           I2C_DutyCycle_2,
           I2C_Ack_Enable,
           I2C_AcknowledgedAddress_7bit);
  I2C_Cmd(I2C1, ENABLE);
}
#endif

#ifdef SSD1306_SPI_CONTROL
/**
  * @brief SPI Peripherial Configuration
  * @retval None
  */
void SPI_Config(void)
{
  SPI_DeInit();

  /* Select the method how NSS is handled */
#ifndef SPI_CS_HARDWARE_CONTROL
  SPI_Init(SPI_FIRSTBIT_MSB,
           SPI_BAUDRATEPRESCALER_2,
           SPI_MODE_MASTER,
           SPI_CLOCKPOLARITY_LOW,
           SPI_CLOCKPHASE_1EDGE,
           SPI_DATADIRECTION_1LINE_TX,
           SPI_NSS_SOFT,
           7);
#else
  SPI_Init(SPI_FIRSTBIT_MSB,
           SPI_BAUDRATEPRESCALER_2,
           SPI_MODE_MASTER,
           SPI_CLOCKPOLARITY_LOW,
           SPI_CLOCKPHASE_1EDGE,
           SPI_DATADIRECTION_1LINE_TX,
           SPI_NSS_HARD,
           7);
#endif

  /* Set the SPI transmit parameters */
  SPI_Cmd(ENABLE);
}
#endif

#ifdef SSD1306_I2C_CONTROL
/**
  * @brief  I2C with Control Byte Transaction
  * @param    Address:      Slave address
  * @param    ControllByte: Controll byte before data
  * @param    pData:        Pointer to block of data
  * @param    DataSize:     How many bytes of data to send
  * @retval None
  */
void I2C_CWrite(uint8_t Address, uint8_t ControlByte, uint8_t *pData, uint16_t DataSize)
{
  /* Check the params */
  if ((pData == NULL) || (DataSize == 0U))
    return;

  /* Check the busy flag */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)) {};

  /* Send START condition and check if uC act as a Master */
  I2C_GenerateSTART(I2C1, ENABLE);
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT)) {};

  /* Send slave address and wait for Slave ACK */
  I2C_Send7bitAddress(I2C1, Address, I2C_Direction_Transmitter);
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) {};

  /* If TX buffer is empty, send a control byte to the Slave
   * and wait for end of transmission
   */
  while((I2C_GetFlagStatus(I2C1, I2C_FLAG_TXE) == RESET)) {};
  I2C_SendData(I2C1, ControlByte);
  while((I2C_GetFlagStatus(I2C1, I2C_FLAG_BTF) == RESET)) {};

  /* Start sending data stream */
  while(DataSize--) {
    /* In case of last byte send NACK */
    if(!DataSize) {
      I2C_AcknowledgeConfig(I2C1, DISABLE);
    }
    I2C_SendData(I2C1, *pData++);
    while((I2C_GetFlagStatus(I2C1, I2C_FLAG_BTF) == RESET)) {};
  }
  /* End of transaction, put STOP condition */
  I2C_GenerateSTOP(I2C1, ENABLE);
}
#endif

#ifdef SSD1306_SPI_CONTROL
/**
  * @brief  SPI Bulk Transaction
  * @param    pData:    Pointer to block of data
  * @param    DataSize: How many bytes of data to send
  * @retval None
  */
void SPI_Transmit(uint8_t *pData, uint16_t DataSize)
{
  /* Check the params */
  if ((pData == NULL) || (DataSize == 0U))
    return;
  /* Do the full transaction */
  while(DataSize--) {
    while(SPI_GetFlagStatus(SPI_FLAG_TXE) == RESET) {};
    SPI_SendData(*pData++);
  }
}
#endif

/* *END OF FILE* */
